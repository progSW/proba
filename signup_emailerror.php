<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>pocetna</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="css/app.css" rel="stylesheet">
        <link href="css/theme-style.css" rel="stylesheet">
        <link href="css/custom-style.css" rel="stylesheet">
        <link href="font-awesome/css/font-awesome.min.css"  rel="stylesheet">
    </head>
    <body>
        <header>
        <div id="undefined-sticky-wrapper" class="sticky-wrapper" style="height: 88px;">
            <nav class="navbar db-navbar" style="">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span> 
                           <span class="icon-bar"></span> 
                           <span class="icon-bar"></span> 
                        </button>
                        <a href="index.php"><img src="images/logo6.jpg" alt="logo"></a>
                    </div>
                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="aboutus.php">AboutUs</a></li>
                            <li><a href="#">Download App</a></li>
                            <li><a href="signup.php"> Sign Up</a></li>
                            <li><a href="login.php"> Login</a></li>
                        </ul>
                    </div>
                </div>
            </nav></div>
        </header> 
        <main class="main-content">            
            <section class="signup-content">                
                <div class="signup-container">                      
                    <div class="row">                                                
                            <div class="col-md-10 col-md-offset-1">
                                <div class="row-content">
                                    <div class="rows">
                                        <div class="col-md-7">
                                             <div class="signup"> </div>                                  
                                        </div>
                                    <div class="col-md-4">
                                        <div class="signup-form">
                                          <h1>Create Account</h1>
                                          <p>Email already exist</p>
                                          <form action="signup1-db.php" method="POST"> 
                                          <?php include('errors.php'); ?>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                <input class="form-control" name="FirstName" placeholder="First Name" required type="text">
                                            </div>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                <input class="form-control" name="LastName" placeholder="Last Name" required type="text">
                                            </div>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                                <input class="form-control" name="Email" placeholder="Email" required type="email">
                                            </div>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                                <input class="form-control" name="Mobile" placeholder="Mobile" required type="text">
                                            </div>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                                <input class="form-control" name="Residence" placeholder="Residence" required type="text">
                                            </div>
                                             <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                                <input class="form-control" name="Pass" placeholder="Password" type="password">
                                            </div>                                            
                                            <div class="form-group text-center">
                                                <p>By proceeding, I agree with Policy Statement.</p>
                                            </div>
                                            <div class="form-group text-center">
                                                <button type="submit" name="reg_user">Get Started</button>
                                            </div>                                            
                                        </form>
                                    </div>
                                    </div>                                
                            </div>
                        </div>
                    </div> 
                    </div>
                    </div>                            
            </section>      
        </main>   
        <footer>            
            <div class="footer-container">                
                <span>Stay in Touch</span>                
                <ul class="list-inline">                     
                    <li><a href="#"><i class="fa fa-facebook-official"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>                     
                </ul>
                <p>LetsCarpooling | Copyright 2019 &copy;</p> 
            </div>
        </footer>
              
        <script src="bootstrap/js/bootstrap.min.js"></script>   
    </body>
</html>



CREATE TABLE users (
  PersonID INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  FirstName VARCHAR(16) NOT NULL,
  LastName VARCHAR(16) NOT NULL,
  Email VARCHAR(16) NOT NULL,
  Mobile VARCHAR(12) NOT NULL,
  Residence VARCHAR(32) NOT NULL,
  Pass VARCHAR(16) UNIQUE NOT NULL,
  PRIMARY KEY(PersonID)
)
ENGINE=InnoDB;

CREATE TABLE rides (
  RidesID INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  RidesName VARCHAR(32) NOT NULL,
  Fromm VARCHAR(32) NOT NULL,
  Too VARCHAR(32) NOT NULL,
  Stops VARCHAR(32) NOT NULL,
  DepartureAt TIMESTAMP NOT NULL,
  ArrivalAt TIMESTAMP NOT NULL,
  PersonID INTEGER UNSIGNED NOT NULL,
  PRIMARY KEY(RidesID),
  INDEX rides_FKIndex1(PersonID),
  FOREIGN KEY(PersonID)
    REFERENCES users(PersonID)
      ON DELETE RESTRICT
      ON UPDATE CASCADE
)
ENGINE=InnoDB;


<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>driver1</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="css/app.css" rel="stylesheet">
        <link href="css/theme-style.css" rel="stylesheet">
        <link href="css/custom-style.css" rel="stylesheet">
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
        
    </head>
    <body>
        <header>
        <div id="undefined-sticky-wrapper" class="sticky-wrapper" style="height: 88px;">
            <nav class="navbar db-navbar" style="">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>                        
                        </button>
                        <a href="index.php"><img src="images/logo6.jpg" alt="logo"></a>
                    </div>
                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="rides.php">MyRides</a></li>
                            <li><a href="#">MyMessages</a></li>                            
                            <li><a href="#">MyProfile</a></li>
                            <li><a href="index.php">SignOut</a></li>                            
                        </ul>
                    </div>
                </div>
            </nav></div>
        </header> 
        <main class="main-content">
            <section class="signup-content">
                <div class="driver-container">                    
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="row-content">
                                <div class="rows">
                                   <div class="col-md-7">
                                        <div class="signup"></div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="signup-form">
                                        <h1>Create Ride</h1>                                        
                                        <form >
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-car"></i></span>
                                                <input class="form-control" name="Name" placeholder="Name" required="" type="text">
                                            </div>
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-map-marker"></span></span>
                                                <input class="form-control" name="Fromm" placeholder="From" required="" type="text">
                                            </div>
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-map-marker"></span></span>
                                                <input class="form-control" name="Too" placeholder="To" required="" type="text">
                                            </div>
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-map-marker"></span></span>
                                                <input class="form-control" name="Stops" placeholder="Stops" required="" type="text">
                                            </div>
                                            <div class="input-group" >
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                <input class="form-control" name="DepartureAt" required="" type="datetime">
                                            </div>
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                <input class="form-control" name="ArrivalAt" required="" type="datetime">
                                            </div>                                                                                        
                                            <!--box for notes dodati-->
                                            <div class="checkbox-inline">
                                                <input type="radio" name="exampleRadios" id="radio" value="radio">
                                                <label>Remind me</label>
                                            </div>
                                            <div class="form-group text-center">
                                                 <button type="submit">Lets Ride</button>
                                            </div>                                            
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                    </div>
                </div>             
            </section>      
        </main> 
        <footer>            
            <div class="footer-container">                
                <span>Stay in Touch</span>                
                <ul class="list-inline">                     
                    <li><a href="#"><i class="fa fa-facebook-official"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>                     
                </ul>
                <p>LetsCarpooling | Copyright 2019 &copy;</p> 
            </div>
        </footer>
              
        <script src="bootstrap/js/bootstrap.min.js"></script>   
    </body>
</html>

